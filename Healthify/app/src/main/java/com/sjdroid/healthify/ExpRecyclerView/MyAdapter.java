package com.sjdroid.healthify.ExpRecyclerView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.sjdroid.healthify.R;
import com.sjdroid.healthify.model.TimeSlot;

import java.util.List;

/**
 * Created by SJ045822 on 2/20/2016.
 */
public class MyAdapter extends ExpandableRecyclerAdapter<SlotParentViewHolder, SlotChildViewHolder> {
    /**
     * Primary constructor. Sets up {@link #mParentItemList} and {@link #mItemList}.
     * <p>
     * Changes to {@link #mParentItemList} should be made through add/remove methods in
     * {@link ExpandableRecyclerAdapter}
     *
     * @param parentItemList List of all {@link ParentListItem} objects to be
     *                       displayed in the RecyclerView that this
     *                       adapter is linked to
     */



    private LayoutInflater mInflator;

    public MyAdapter(Context context,List<? extends ParentListItem> parentItemList) {
        super(parentItemList);

        mInflator = LayoutInflater.from(context);

    }

    @Override
    public SlotParentViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {

        View slotparentView= mInflator.inflate(R.layout.slotparent,parentViewGroup,false);
        return  new SlotParentViewHolder(slotparentView);


    }



    @Override
    public SlotChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {

        View slotChildView= mInflator.inflate(R.layout.slotchild, childViewGroup,false);
        return  new SlotChildViewHolder(slotChildView);

    }




    @Override
    public void onBindParentViewHolder(SlotParentViewHolder parentViewHolder, int position, ParentListItem parentListItem) {

        SlotParent mSlotParent= (SlotParent) parentListItem;
        parentViewHolder.bind(mSlotParent);

    }



    @Override
    public void onBindChildViewHolder(SlotChildViewHolder childViewHolder, int position, Object childListItem) {

        TimeSlot mTimeSlot = (TimeSlot) childListItem;
        childViewHolder.bind(mTimeSlot);


    }
}
