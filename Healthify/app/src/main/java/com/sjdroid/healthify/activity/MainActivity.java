package com.sjdroid.healthify.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.sjdroid.healthify.HealthifyAPI;
import com.sjdroid.healthify.R;
import com.sjdroid.healthify.model.Slot;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://108.healthifyme.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HealthifyAPI mHealthifyAPI = retrofit.create(HealthifyAPI.class);


        // Asynchronous Call in Retrofit 2.0

        Call<Slot> call = mHealthifyAPI.get_slots();
        call.enqueue(new Callback<Slot>() {
            @Override
            public void onResponse(Call<Slot> call, Response<Slot> response) {



                Log.i("response", response.body().toString());

                Slot mSlots=response.body();
                BookSlotsActivity.startActivity(mSlots, MainActivity.this);
            }


            @Override
            public void onFailure(Call<Slot> call, Throwable t) {
               Log.i("message", t.getMessage());
            }
        });

    }
}