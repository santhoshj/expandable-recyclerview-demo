package com.sjdroid.healthify.model;

import com.sjdroid.healthify.model.DaySlots;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by SJ045822 on 2/19/2016.
 */
public class Slot implements Serializable {

    public HashMap<String,DaySlots> slots;

    @Override
    public String toString() {


        String id="";


        for (Map.Entry<String,DaySlots> day: slots.entrySet()) {
            id+=", "+day.getKey()+day.getValue();
        }
        
        return id;
    }
}
