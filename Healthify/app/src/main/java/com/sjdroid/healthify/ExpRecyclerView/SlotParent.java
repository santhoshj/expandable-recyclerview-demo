package com.sjdroid.healthify.ExpRecyclerView;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.sjdroid.healthify.model.TimeSlot;

import java.util.List;

/**
 * Created by SJ045822 on 2/20/2016.
 */
public class SlotParent implements ParentListItem {

    String title;
    List <TimeSlot> mTimeSlotlist;

   public  SlotParent(String title, List<TimeSlot> timeSlot){
        mTimeSlotlist=timeSlot;
        this.title = title;
    }

    @Override
    public List<TimeSlot> getChildItemList() {
        return  mTimeSlotlist;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
