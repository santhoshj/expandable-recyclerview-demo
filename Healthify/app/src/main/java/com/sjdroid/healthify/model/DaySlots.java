package com.sjdroid.healthify.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by SJ045822 on 2/19/2016.
 */
public class DaySlots implements  Serializable{

    List<TimeSlot> morning;
    List<TimeSlot> afternoon;
    List<TimeSlot> evening;

    public List<TimeSlot> getMorning() {
        return morning;
    }
    //HashMap<String,List<TimeSlot>> test;


    public List<TimeSlot> getAfternoon() {
        return afternoon;
    }

    public List<TimeSlot> getEvening() {
        return evening;
    }

    @Override
    public String toString() {
        return "DaySlots{" +
                "morning=" + morning +
                ", afternoon=" + afternoon +
                ", evening=" + evening +
                '}';
    }


}
