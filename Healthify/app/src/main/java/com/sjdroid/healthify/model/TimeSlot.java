package com.sjdroid.healthify.model;

import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by SJ045822 on 2/19/2016.
 */
public class TimeSlot implements Serializable {

    String end_time;
    String start_time;
    boolean is_booked;
    boolean is_expired;
    int slot_id;

    public String getEnd_time() {

        String str=end_time.substring(10,16);
        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
        Date _24HourDt = null;
        try {
            _24HourDt = _24HourSDF.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        str=_12HourSDF.format(_24HourDt);

        return str;
    }

    public String getStart_time() {


        String str=start_time.substring(10,16);

        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
        Date _24HourDt = null;
        try {
            _24HourDt = _24HourSDF.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        str=_12HourSDF.format(_24HourDt);

        return str;
    }

    public boolean is_booked() {
        return is_booked;
    }

    public boolean is_expired() {
        return is_expired;
    }

    public int getSlot_id() {
        return slot_id;
    }

    @Override
    public String toString() {
        return "TimeSlot{" +
                "end_time='" + end_time + '\'' +
                ", start_time='" + start_time + '\'' +
                ", is_booked=" + is_booked +
                ", is_expired=" + is_expired +
                ", slot_id=" + slot_id +
                '}';
    }


    public String get_timeSlot() {
        return getStart_time() + " - " + getEnd_time();


    }

}


