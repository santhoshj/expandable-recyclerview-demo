package com.sjdroid.healthify.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sjdroid.healthify.ExpRecyclerView.MyAdapter;
import com.sjdroid.healthify.R;
import com.sjdroid.healthify.ExpRecyclerView.SlotParent;
import com.sjdroid.healthify.model.DaySlots;
import com.sjdroid.healthify.model.Slot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BookSlotsActivity extends AppCompatActivity {

    private static final String KEY_SLOTS = "slots";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    TabLayout mTabLayout;


    Slot mSlots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_slots);
        mSlots = (Slot) getIntent().getExtras().getSerializable(KEY_SLOTS);

        List<SlotParent> slotParents = new ArrayList<>();

        HashMap<String, DaySlots> dateSlots = mSlots.slots;


        for (String date : dateSlots.keySet()) {
            DaySlots slots = dateSlots.get(date);


            SlotParent slotParent = new SlotParent("Morning", slots.getMorning());
            if (slots != null)
                slotParents.add(slotParent);


            slotParent = new SlotParent("Afternoon", slots.getAfternoon());
            if (slotParent != null)
                slotParents.add(slotParent);


            slotParent = new SlotParent("Evening", slots.getEvening());
            if (slotParent != null)
                slotParents.add(slotParent);


        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        setSupportActionBar(toolbar);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        for (SlotParent slotParent : slotParents) {
            Log.i("slotvale", slotParent.toString());

        }

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), dateSlots);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mTabLayout.setupWithViewPager(mViewPager);
    }


    public static void startActivity(Slot mSlots, Context context) {
        Intent intent = new Intent(context, BookSlotsActivity.class);
        Bundle bundle = new Bundle();
        Log.i("position", "startactivity");
        bundle.putSerializable(KEY_SLOTS, mSlots);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static List<SlotParent> mslotParent;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */


        public static PlaceholderFragment newInstance(int sectionNumber, List<SlotParent> slotParent) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            mslotParent = slotParent;
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_book_slots, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview1);

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            MyAdapter adapter = new MyAdapter(getActivity(), mslotParent);
            recyclerView.setAdapter(adapter);

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {


        HashMap<String, DaySlots> mDaySlots;

        List<String> dates = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm, HashMap<String, DaySlots> slotParents) {
            super(fm);
            mDaySlots = slotParents;

            for (String s : mDaySlots.keySet()) {
                dates.add(s);
            }

        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.


            // Return a PlaceholderFragment (defined as a static inner class below).

            DaySlots slots = mDaySlots.get(dates.get(position));
            List<SlotParent> slotParents = new ArrayList<>();


            SlotParent slotParent = new SlotParent("Morning", slots.getMorning());
            slotParents.add(slotParent);

            slotParent = new SlotParent("Afternoon", slots.getAfternoon());

            slotParents.add(slotParent);

            slotParent = new SlotParent("Evening", slots.getEvening());
            slotParents.add(slotParent);
            return PlaceholderFragment.newInstance(position + 1, slotParents);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return mDaySlots.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
           /* switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
           */

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            Date date = null;
            try {

                date = formatter.parse(dates.get(position));

            } catch (ParseException e) {
                e.printStackTrace();
            }

            String str = null;
            if (date != null) {
                str = String.valueOf(date.getDate());
            }

            String[] weekdays = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
            String str1 = null;
            if (date != null) {
                str1 = weekdays[date.getDay()];
            }

            String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            String str2 = null;
            if (date != null) {
                str2 = monthNames[date.getMonth()];
            }

            TextView mMonthView = (TextView) findViewById(R.id.monthview);
            mMonthView.setText(str2);

            return str + "\n" + str1;
        }
    }
}
