package com.sjdroid.healthify.ExpRecyclerView;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.sjdroid.healthify.R;

/**
 * Created by SJ045822 on 2/20/2016.
 */
public class SlotParentViewHolder extends ParentViewHolder {
    private  TextView slotItemView;
    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */


    private TextView periodView;

    public SlotParentViewHolder(View itemView) {
        super(itemView);
        periodView = (TextView) itemView.findViewById(R.id.period);
        slotItemView = (TextView) itemView.findViewById(R.id.availableslots);

    }


    public void bind(SlotParent slotParent){
    periodView.setText(slotParent.title);
    slotItemView.setText(slotParent.getChildItemList().size()+" slots available");

    }
}
