package com.sjdroid.healthify.ExpRecyclerView;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.sjdroid.healthify.R;
import com.sjdroid.healthify.model.TimeSlot;

/**
 * Created by SJ045822 on 2/20/2016.
 */
public class SlotChildViewHolder extends ChildViewHolder {
    private TextView timePeriodView;
    private TextView slotItemView;

    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */

    public SlotChildViewHolder(View itemView) {
        super(itemView);
        timePeriodView = (TextView) itemView.findViewById(R.id.time_period);

    }

    public void bind(TimeSlot timeSlot) {

        timePeriodView.setText(timeSlot.get_timeSlot());




        if(timeSlot.is_expired()){
            Log.i("exprire","true");
            if(timeSlot.is_booked()){
                timePeriodView.setBackgroundColor(Color.parseColor("#DCDCDC"));
            }
            else
                timePeriodView.setBackgroundColor(Color.parseColor("#808080"));
        }


    }

}
