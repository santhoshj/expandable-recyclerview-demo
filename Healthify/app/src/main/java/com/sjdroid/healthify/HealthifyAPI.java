package com.sjdroid.healthify;


import com.sjdroid.healthify.model.Slot;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by SJ045822 on 2/18/2016.
 */
public interface HealthifyAPI {

  @GET("booking/slots/all?username=alok%40x.coz&api_key=a4aeb4e27f27b5786828f6cdf00d8d2cb44fe6d7&vc=276&expert_username=neha%40healthifyme.com&format=json")
  Call<Slot> get_slots();


}
